#! /bin/bash

rm -rf out
mkdir out
pandoc -o out/adventure.epub --css style.css body_text_e.md
pandoc --template template.html body_text_w.md > out/index.html
cp style.css out/
cp hexmap.png out/
